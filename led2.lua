-- init the ws2812 module
ws2812.init(ws2812.MODE_SINGLE)
-- create a buffer, 60 LEDs with 3 color bytes
strip_buffer = ws2812.newBuffer(60, 3)
-- init the effects module, set color to red and start blinking
ws2812_effects.init(strip_buffer)
ws2812_effects.set_speed(25)
ws2812_effects.set_brightness(10)
tmr.create():alarm(25, 1, function()
  ws2812_effects.set_color( math.random(0,255), math.random(0,255), math.random(0,255))
end)
ws2812_effects.set_color(50,255,025)
ws2812_effects.set_delay(25)


ws2812_effects.set_mode("flicker",50)

 


ws2812_effects.start()
