dofile("security.lua")
dofile("functions.lua")
station_cfg={}
station_cfg.ssid=SECURITY_wifi_ssid
station_cfg.pwd=SECURITY_wifi_password
station_cfg.save=true
wifi.setmode(wifi.STATION,true)
wifi.sta.config(station_cfg)

cpufan=6
backfan=7
topfan=5
frontfan=8
pwm.setup(5, 300, 800)--back
pwm.setup(6, 300, 800)--cpu
pwm.setup(7, 300, 800)--up
pwm.setup(8, 300, 800)--front
if file.exists("fan.arg") then
    f=file.open("fan.arg","r")
    pwm.setduty(cpufan,1023-string.gsub(f:readline(), "n", ""))--cpu
    pwm.setduty(backfan,1023-string.gsub(f:readline(), "n", ""))--back
    pwm.setduty(topfan,1023-string.gsub(f:readline(), "n", ""))--up
    pwm.setduty(frontfan,1023-string.gsub(f:readline(), "n", ""))--front
    print(pwm.getduty(cpufan).." - "    ..pwm.getduty(topfan).." - "    ..pwm.getduty(backfan).." - "..pwm.getduty(frontfan))
end
pwm.start(5)
pwm.start(6)
pwm.start(7)
pwm.start(8)

if abortled == true then
else
    dofile("led.lua")
end
gpio.write(1, gpio.LOW)
gpio.mode(1, gpio.OUTPUT)
gpio.mode(2, gpio.INPUT)

turnoff=0
togglePowerTimer = tmr.create()
togglePowerTimer:register(500, tmr.ALARM_SEMI, function (t)
    if turnoff==0 then
        gpio.write(1, gpio.LOW)
    elseif turnoff==1 then
        gpio.write(1, gpio.LOW)
        turnoff=2
        togglePowerTimer:start()
    elseif turnoff==2 then
        gpio.write(1, gpio.HIGH)
        turnoff=0
        togglePowerTimer:start()
    end
end)

dofile("serv.lua")

