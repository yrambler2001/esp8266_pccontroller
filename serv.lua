srv = net.createServer(net.TCP, 28800)
print("Server created... \n")
pinState = 0
srv:listen(SECURITY_port, function(conn)
    conn:on("receive", function(sck, request)
        print ("new packet recieved")
        local _POST = {}
        if (request~=nil) then
            for k, v in string.gmatch(request, "(%w+)=(%w+)&*") do
                _POST[k] = v
                print(k..' key-value '..v) end
        end
        local message = {}
        local outpost = {}

        message[#message + 1] = "HTTP/1.1 200 OK\r\n"
        message[#message + 1] = "Content-Type: text/html\r\n\r\n"

        if (_POST.psw==SECURITY_password) then
            if(_POST.power == "toggle")then
                turnoff=gpio.read(2)
                gpio.write(1, gpio.HIGH);
                togglePowerTimer:start()
            end
            if (_POST.fan0 ~= nil)and(_POST.fan1 ~= nil)and(_POST.fan2 ~= nil)and(_POST.fan3 ~= nil) then
                updateFile("fan.arg",_POST.fan0.."\n".._POST.fan1.."\n".._POST.fan2.."\n".._POST.fan3)
                pwm.setduty(cpufan,1023-_POST.fan0)--cpu
                pwm.setduty(backfan,1023-_POST.fan1)--back
                pwm.setduty(topfan,1023-_POST.fan2)--up
                pwm.setduty(frontfan,1023-_POST.fan3)--front

            end
            if(_POST.led ~= nil) then
                updateFile("led.arg",_POST.led)
            end
             if(_POST.ledcolor ~= nil) then
                updateFile("ledcolor.arg",_POST.ledcolor)
                sColo=_POST.ledcolor
                aiColo=hextoarr(sColo)
            end
            if(_POST.ledfunc ~= nil) then
                updateFile("ledfunc.arg",_POST.ledfunc)
                aFunc=_POST.ledfunc
            end

            if(_POST.doit == "info")then
            end
            outpost[#outpost+1]='NP_PC='..gpio.read(2)..'_PN'
            if file.open("led.arg") then
                outpost[#outpost+1] = "NP_led="..file.read().."_PN"
                file.close()
            end
            outpost[#outpost+1] = "NP_ledcolor="..sColo.."_PN"
            outpost[#outpost+1] = "NP_ledfunc="..aFunc.."_PN"
                outpost[#outpost+1] = "NP_fan="..1023-pwm.getduty(cpufan).."A"..1023-pwm.getduty(backfan).."A"..1023-pwm.getduty(topfan).."A"..1023-pwm.getduty(frontfan).."_PN"
        end
        message[#message + 1] = '\r\n\r\n'
        while #outpost > 0 do
            message[#message + 1] = table.remove(outpost, 1) end

        outpost=nil
        local function send(sk)
            if #message > 0 then
                sk:send(table.remove(message, 1).." ")
            else
                sk:close()
                message = nil
                print("Heap Available:" .. node.heap())
            end
        end
        sck:on("sent", send)
        send(sck)
    end)
end)
