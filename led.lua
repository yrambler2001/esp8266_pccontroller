dofile("colours.lua")
dofile("ledfunctions.lua")
ws2812.init()

function pseudoSin(idx)
    idx = idx % 128
    lookUp = 32 - idx % 64
    val = 256 - (lookUp * lookUp) / 4

    if (idx > 64) then
        val = -val
    end

    return 256 + val
end

function hextoarr(sHex)
    return {tonumber(string.sub(sHex, 1, 2), 16), tonumber(string.sub(sHex, 3, 4), 16), tonumber(string.sub(sHex, 5, 6), 16)}
end

sColo=rFile("ledcolor.arg","ffffff")
aiColo = hextoarr(sColo)
aFunc=rFile("ledfunc.arg","1")
i = 0
ii = 128 / 2
dir = 0
a = 0
aa = 0
aaa = 0
b = 0
bb = 0
bbb = 0
timz = 0
maxx = 254
q = 1


buffer = ws2812.newBuffer(48 + 28, 3)

buffer:fill(0, 0, 0)

tmr.create():alarm(25, 1, function()
    if aFunc == "1" then
        do
            i = tst(i + 1, 1)
            ii = tst(ii + 1, 2)
            buffer:fill(ts1(a * ts(i) + b * ts(ii)), ts1(aa * ts(i) + bb * ts(ii)), ts1(aaa * ts(i) + bbb * ts(ii)))
            ws2812.write(buffer)
        end
    else
        if aFunc == "2" then
            do
                buffer:fill(aiColo[2], aiColo[1], aiColo[3])
                ws2812.write(buffer)
            end
        end
    end
end)
